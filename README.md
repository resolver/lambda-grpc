# Lambda + EC2 + NLB + gRPC + Node issue

## Overview

Grpc clients are created ( request creation ) within Lambda handler scope and it is known that grpc optimizes and reuses connections somehow in its implementation. Services use grpc server max-age to open/close connections/channels and therefore, services do not explicitly call .close() on clients/channels. It works just fine within services running on ECS instances ( service to service ). However, the connections never get released in the servers when Lambdas create the client with the server. The connections in the server get stuck in FIN_WAIT2 waiting for final FIN from clients that never happen.

## EC2

### Instalattion

- Create an EC2 instance using Amazon Linux AMI or compatible.

- Add 30052 TCP port to Security Group Inbound rules allowing either 0.0.0.0/0 or your subnet ( same as NLB and Lambda ) traffic to reach the instance.

- Add 22 TCP port to Security Group Inbound rules ( SSH ).

- SSH into the instance ( or you can check the command by clicking on "Connect" button on your EC2 Dashboard ).
    
    ```
    ssh -i "[your-key-if-any]" ec2-user@[instance-ip-here]
    ```

- Install docker
    
    ```
    https://docs.aws.amazon.com/AmazonECS/latest/developerguide/docker-basics.html
    ```

- Install git
    
    ```
    sudo yum install git
    ```

- Clone this repository.
    
    ```
    git clone https://felipecamposresolver@bitbucket.org/resolver/lambda-grpc.git
    ```

- cd into the project folder.
    
    ```
    cd ./lambda-grpc/ec2-src/
    ```

- Build docker container. ( using `node:8` base image )
    
    ```
    docker build . -t server-grpc
    ```

- Run the container ( default server port is 45000  ). We bind port 45000 of the container to TCP port 30052.
    
    ```
    docker run -d -p 30052:45000 server-grpc
    ```

## NLB
I won't go in much details here but make sure to create a "Network Load Balancer" and create a listener to TCP port 30052. Create a target group with protocol TCP, port 30052 and add it as "Default action -> Forward to [your_target_group]" in your listener. You will need `DNS name` to configure in your Lambda.

## Lambda

This step-by-step assumes you are using Linux/macOs based systems and you have `git` already installed.

### Installation

- Clone this repository.
    
    ```
    git clone https://felipecamposresolver@bitbucket.org/resolver/lambda-grpc.git
    ```

- cd into the project folder.
    
    ```
    cd ./lambda-grpc/lambda-src/
    ```

- Generate lambda zip bundle
    
    ```
    sh ./script.sh
    ```

- Create a Lambda and select "Author from scratch", give it a name, select "Runtime" to "Node.js 8.10" and create/select an execution role that has access to resources you will need to run the function.

- Upload bundle ( `lambda-grpc.zip` ) to Lambda.

- Add an environment variable SERVER_HOST=[NLB_dns_name_here] in your Lambda.

- Save the changes.


## How to reproduce?

- Configure a test event in your lambda and use the following payload:
    
    ```
    {
    "org": "1"
    }
    ```

- Click the button test and wait for the response.

- Connect to your EC2 instance and grab the `CONTAINER_ID` of your running container.
    
    ```
    docker ps |grep server-grpc
    ```

- Check the network connections in your container
    
    ```
    docker exec -it [contianer_id] netstat -tan
    ```

- You should now see 1 `ESTABLISHED` connection that will become `FIN_WAIT2` after a few seconds.

## Notes
 - `service.load` function uses a delay to simulate a real response time.
 - `service.load` has 1 out of 10 chances to return an error to simulate exceptions ( which should not cause issues ).