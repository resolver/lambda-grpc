const grpc = require('grpc');
const path = require('path');
const proto = grpc.load(path.resolve(__dirname, '../proto/service.proto')).ServiceA;
const service = require('../service');

const register = (server) => {
    server.addService(proto.Service.service, {
        load: (call, callback) => {
            const payload = call.request;
            console.log('load handler payload : ', payload);
            service.load(payload.org)
                .then((result) => {
                    console.log('succes, result : ', result);
                    callback(null, { reply: result });
                })
                .catch((err) => {
                    console.log('error, message : ', err.message);
                    callback(err, null);
                });
        },
    });
};

module.exports = { register };