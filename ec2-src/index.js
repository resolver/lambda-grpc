const _ = require('lodash');
const grpc = require('grpc');

const serverHost = process.env.SERVER_HOST || '0.0.0.0:45000';

const startServer = (config, handlers) => {
    // GRPC config options: https://github.com/grpc/grpc/blob/master/include/grpc/impl/codegen/grpc_types.h
    const server = new grpc.Server({
        'grpc.max_send_message_length': 104857600,
        'grpc.max_receive_message_length': 104857600,
        'grpc.max_connection_idle_ms': 15000,
        'grpc.max_connection_age_ms': 30000,
        'grpc.keepalive_time_ms': 5000,
        'grpc.keepalive_timeout_ms': 1000,
        'grpc.keepalive_permit_without_calls': 1
    });
    handlers.forEach(handler => {
        handler.register(server);
    });
    server.bind(config.serverHost, grpc.ServerCredentials.createInsecure());
    server.start();
};

const initRPC = async () =>  {
    console.log('Server host => ', serverHost);
    return startServer(
        { serverHost },
        [ require('./handlers/handler') ]
    );
};

initRPC();