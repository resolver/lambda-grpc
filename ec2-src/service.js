const Promise = require('bluebird');
const _ = require('lodash');

const minFnDelay = process.env.MIN_FN_DELAY || 0;
const maxFnDelay = process.env.MAX_FN_DELAY || 3000;

const load = (org) => {
    // Simulate a delay
    return Promise.delay(_.random(minFnDelay, maxFnDelay)).then(() => {
        if (_.random(1, 10) === 5) { // add a chance of error, 1 out of 10 ( 10% chance )
            throw new Error(`You're unlucky. It's an error.`);
        }
        return `load return ${org}`;
    });
};

module.exports = {
    load
};