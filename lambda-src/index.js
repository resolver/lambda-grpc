const { getClient, callMethod } = require('./grpc');

exports.handler = () => {
    return callMethod(getClient(process.env.SERVER_HOST), 'load', { org: 1 });
};