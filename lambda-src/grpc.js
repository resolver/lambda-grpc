'use strict';

const _ = require('lodash');
const grpc = require('grpc');
const path = require('path');
const Promise = require('bluebird');
const proto = grpc.load(path.resolve(__dirname, './proto/service.proto')).ServiceA;

let clients = [];

const shouldRetry = (err) => err.code === grpc.status.UNAVAILABLE;

const callMethod = (client, handler, payload) => {
    let clientFunc = () => Promise.resolve();
    try {
        const fnAsync = Promise.promisify(client[_.camelCase(handler)], { context: client });
        clientFunc = () => fnAsync(payload);
    } catch (e) {
        console.error(`grpc callMethod missing client handler function: "${handler}"`);
        throw e;
    }

    const retryLimit = 3;
    const retryDelay = 500;

    const retry = (fn, limit = 3, delay = 500) => {
        return new Promise((resolve, reject) => {
            fn()
                .then(resolve)
                .catch((error) => {
                    const sRetry = shouldRetry(error);
                    if (!sRetry) {
                        reject(error);
                        return;
                    } else if (sRetry && limit <= 1) {
                        console.debug(`grpc callMethod ${handler} failed with UNAVAILABLE, reached max number of retries ( max: ${retryLimit} )`);
                        reject(error);
                        return;
                    }
                    setTimeout(() => {
                        console.debug(`grpc callMethod ${handler} failed with UNAVAILABLE, retrying in ${delay} ms... ( count: ${retryLimit - limit + 1} )`);
                        retry(fn, limit - 1, delay).then(resolve, reject);
                    }, delay);
                });
        });
    };

    return retry(clientFunc, retryLimit, retryDelay)
        .then(results => results);
};

const getClient = (serverHost) => {
    return createClient(serverHost, proto.Service);
};

const createClient = function(serverHost, protoService) {
    console.log('create a new client');
    const channelOptions = {
        'grpc.max_send_message_length': 104857600,
        'grpc.max_receive_message_length': 104857600,
        'grpc.keepalive_time_ms': 5000,
        'grpc.keepalive_timeout_ms': 1000,
        'grpc.keepalive_permit_without_calls': 1,
    };

    let client = new protoService(serverHost, grpc.credentials.createInsecure(), channelOptions);

    clients.push(client);

    return client;
};

const closeClients = () => {
    _.each(clients, (client) => client.close());
};

module.exports = {
    callMethod,
    getClient,
    closeClients
};