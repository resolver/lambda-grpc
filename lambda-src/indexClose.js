const { getClient, callMethod, closeClients } = require('./grpc');

exports.handler = async () => {
    const res = await callMethod(getClient(process.env.SERVER_HOST), 'load', { org: 1 });
    closeClients();
    return res;
};