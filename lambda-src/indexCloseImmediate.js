const { getClient, callMethod, closeClients } = require('./grpc');

exports.handler = async () => {
    const res = await callMethod(getClient(process.env.SERVER_HOST), 'load', { org: 1 });
    closeClients();
    // return new Promise((resolve) => setTimeout(() => { resolve(res); }, 0)); // it doesn't work
    return new Promise((resolve) => setImmediate(() => resolve(res)));
};